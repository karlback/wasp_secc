
import numpy as np
import Conditions
from Geometry import Point

NOTUSED = "NOTUSED"

ORR = "ORR"

ANDD = "ANDD"


def decide(CMV, LCM, PUV):

    ###
    # calculate PUM
    cond_dim = len(CMV)
    PUM = np.zeros([cond_dim, cond_dim])
    for i in range(cond_dim):
        for j in range(cond_dim):
            PUM[i][j] = False  # default
            if LCM[i][j] == ANDD:
                PUM[i][j] = CMV[i] and CMV[j]
            elif LCM[i][j] == ORR:
                PUM[i][j] = CMV[i] or CMV[j]
            elif LCM[i][j] == NOTUSED:
                PUM[i][j] = True
    FUV = [False] * cond_dim
    for i in range(cond_dim):
        if not PUV[i]:
            FUV[i] = True
        else:
            FUV[i] = np.all(PUM, 0)[i]
    out = np.all(FUV)
    return out, PUM, FUV


def get_CMV(num_points, parameters, points):
    CMV = []
    # 0
    cond0 = Conditions.Cond0(parameters, num_points, points)
    CMV.append(cond0.eval())
    # 1
    cond1 = Conditions.Cond1(parameters, num_points, points)
    CMV.append(cond1.eval())
    # 2
    cond2 = Conditions.Cond2(parameters, num_points, points)
    CMV.append(cond2.eval())
    # 3
    cond3 = Conditions.Cond3(parameters, num_points, points)
    CMV.append(cond3.eval())
    # 4
    cond4 = Conditions.Cond4(parameters, num_points, points)
    CMV.append(cond4.eval())
    # 5
    cond5 = Conditions.Cond5(parameters, num_points, points)
    CMV.append(cond5.eval())
    return CMV