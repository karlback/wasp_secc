
import unittest
import numpy as np

import Conditions
from decide import *
from Geometry import Point

class GeometryMethods(unittest.TestCase):

    def test_norm(self):
        # Intent: Verify that the norm function works as expected for vectors with positive, negative and zero -components
        p0 = Point(0,0)
        self.assertTrue( p0.norm() == 0 )
        p1 = Point(1,0)
        self.assertTrue( p.norm() == 1 )
        p = Point(-1,4)
        self.assertTrue( p.norm() >= 0 )
        p = Point(4,-20)
        self.assertTrue( p.norm() >= 0 )

    def test_subtract(self):
        # Intent: Verify that vector subtraction behaves appropriately, and that the norm is unchanged when subtracting the zero vector
        p0 = Point(0,0)
        p1 = Point(1,2)
        self.assertTrue( p1.subtract(p0).norm() == p1.norm() )
        self.assertTrue( p0.subtract(p1).norm() == p1.norm() )
        p2 = Point(10,2)
        p3 = Point(5,2)
        self.assertTrue( p2.subtract(p3).norm() == 5 )

class ConditionEvaluations(unittest.TestCase):

    def test_cond0(self):
        # Intent: Verify that the distance condition (0) is fulfilled for nominal and corner cases, as well as for some cases which are easily verified

        parameters = {
            'length1': 0
        }

        cond = Conditions.Cond0(parameters, 0, [])
        self.assertTrue( not cond.eval() )

        cond = Conditions.Cond0(parameters, 1, [Point(0,0)])
        self.assertTrue( not cond.eval() )

        cond = Conditions.Cond0(parameters, 2, [Point(0,0), Point(0,0)])
        self.assertTrue( not cond.eval() )

        cond = Conditions.Cond0(parameters, 2, [Point(0,0), Point(1,0)])
        self.assertTrue( cond.eval() )

        parameters = {
            'length1': 100
        }

        cond = Conditions.Cond0(parameters, 2, [Point(0,0), Point(1,0)])
        self.assertTrue( not cond.eval() )

        cond = Conditions.Cond0(parameters, 2, [Point(0,0), Point(101,0)])
        self.assertTrue( cond.eval() )

        cond = Conditions.Cond0(parameters, 2, [Point(0,0), Point(100,1)])
        self.assertTrue( cond.eval() )

    def test_cond1(self):
        # Intent: Verify that the bounding circle condition (1) behaves as expected for too few points, some corner cases, as well as for some easily verifiable cases

        parameters = {
            'radius1': np.Inf
        }

        cond = Conditions.Cond1(parameters, 0, [])
        self.assertTrue( not cond.eval() )

        cond = Conditions.Cond1(parameters, 1, [Point(0,0)])
        self.assertTrue( not cond.eval() )

        cond = Conditions.Cond1(parameters, 2, [Point(0,0), Point(0,0)])
        self.assertTrue( not cond.eval() )

        cond = Conditions.Cond1(parameters, 3, [Point(0,0), Point(0,0), Point(0,0)])
        self.assertTrue( cond.eval() )

        parameters = {
            'radius1': 0
        }

        cond = Conditions.Cond1(parameters, 3, [Point(0,0), Point(0,0), Point(0,0)])
        self.assertTrue( cond.eval() )

        parameters = {
            'radius1': 1
        }

        cond = Conditions.Cond1(parameters, 3, [Point(0,0), Point(0,1), Point(1,0)])
        self.assertTrue( cond.eval() )

    def test_cond2(self):
        # Intent: Verify that the angle condition (2) behaves as expected, fails for few points, fails for duplicate points, works when expected, and fails for an impossible angle criterion

        parameters = {
            'epsilon': 0
        }

        cond = Conditions.Cond2(parameters, 0, [])
        self.assertTrue( not cond.eval() )

        cond = Conditions.Cond2(parameters, 1, [Point(0,0)])
        self.assertTrue( not cond.eval() )

        cond = Conditions.Cond2(parameters, 2, [Point(0,0), Point(0,0)])
        self.assertTrue( not cond.eval() )

        cond = Conditions.Cond2(parameters, 3, [Point(0,0), Point(0,0), Point(0,0)])
        self.assertTrue( not cond.eval() )

        cond = Conditions.Cond2(parameters, 3, [Point(0,0), Point(1,0), Point(1,1)])
        self.assertTrue( cond.eval() )

        cond = Conditions.Cond2(parameters, 3, [Point(0,0), Point(1,0), Point(1,10**-31)])
        self.assertTrue( cond.eval() )

        parameters = {
            'epsilon': np.pi
        }

        cond = Conditions.Cond2(parameters, 3, [Point(1,3), Point(2,4), Point(3,-1)])
        self.assertTrue( not cond.eval() )

        cond = Conditions.Cond2(parameters, 3, [Point(-4,2), Point(1,-1), Point(-5,99)])
        self.assertTrue( not cond.eval() )

    def test_cond3(self):
        # Intent: Verify that the area criterion (3) fails for few points, and when expected, and works for verifiably correct cases

        parameters = {
            'area1': 0
        }

        cond = Conditions.Cond3(parameters, 0, [])
        self.assertTrue( not cond.eval() )

        cond = Conditions.Cond3(parameters, 1, [Point(0,0)])
        self.assertTrue( not cond.eval() )

        cond = Conditions.Cond3(parameters, 2, [Point(0,0), Point(0,0)])
        self.assertTrue( not cond.eval() )

        cond = Conditions.Cond3(parameters, 2, [Point(0,0), Point(1,2)])
        self.assertTrue( not cond.eval() )

        cond = Conditions.Cond3(parameters, 3, [Point(0,0), Point(0,0), Point(0,0)])
        self.assertTrue( not cond.eval() )

        cond = Conditions.Cond3(parameters, 3, [Point(0,0), Point(1,0), Point(2,0)])
        self.assertTrue( not cond.eval() )

        cond = Conditions.Cond3(parameters, 3, [Point(0,0), Point(1,0), Point(2,1)])
        self.assertTrue( cond.eval() )

        cond = Conditions.Cond3(parameters, 3, [Point(1,4), Point(-21,90), Point(200,11)])
        self.assertTrue( cond.eval() )

        parameters = {
            'area1': 2
        }

        cond = Conditions.Cond3(parameters, 3, [Point(0,0), Point(2,0), Point(2,2)])
        self.assertTrue( not cond.eval() )

        parameters = {
            'area1': np.Inf
        }

        cond = Conditions.Cond3(parameters, 3, [Point(10,-15), Point(10**7,-10**7), Point(-10**7,10**7)])
        self.assertTrue( not cond.eval() )

    def test_cond4(self):
        # Intent: Verify that the quadrant condition (4) fails for few points, behaves as expected in ambigious cases, and works in verifiably correct cases

        parameters = {
            'q_pts': 0,
            'quads': 0
        }

        cond = Conditions.Cond4(parameters, 0, [])
        self.assertTrue( not cond.eval() )

        parameters = {
            'q_pts': 1,
            'quads': 0
        }

        cond = Conditions.Cond4(parameters, 0, [])
        self.assertTrue( not cond.eval() )

        cond = Conditions.Cond4(parameters, 1, [Point(0,0)])
        self.assertTrue( cond.eval() )

        cond = Conditions.Cond4(parameters, 1, [Point(-1,1)])
        self.assertTrue( cond.eval() )

        parameters = {
            'q_pts': 4,
            'quads': 3
        }

        cond = Conditions.Cond4(parameters, 4, [Point(0,0), Point(0,-1), Point(-1,0), Point(-2,0)])
        self.assertTrue( not cond.eval() )

        cond = Conditions.Cond4(parameters, 4, [Point(0,0), Point(0,-1), Point(-1,0), Point(-1,1)])
        self.assertTrue( cond.eval() )

    def test_cond5(self):
        # Intent: Verify that the delta-x condition (5) is correct for x-coordinates of different signs, and fails for few points

        cond = Conditions.Cond5({}, 0, [])
        self.assertTrue( not cond.eval() )

        cond = Conditions.Cond5({}, 1, [Point(0,0)])
        self.assertTrue( not cond.eval() )

        cond = Conditions.Cond5({}, 2, [Point(0,0), Point(1,0)])
        self.assertTrue( not cond.eval() )

        cond = Conditions.Cond5({}, 2, [Point(0,0), Point(-1,0)])
        self.assertTrue( cond.eval() )

class DecideFunctionality(unittest.TestCase):

    def test_CMV(self):
        # Intent: Since the behaviour of the evaluation of the conditions is tested above, here the output format of the CMV function is verified

        parameters = {
            'length1': 2,
            'radius1': 2,
            'epsilon': 0.001,
            'area1': 1.5,
            'q_pts': 3,
            'quads': 2
        }

        num_points = 5
        points = [Point(1, 7), Point(2, 5), Point(3, 1), Point(4, 3), Point(5, 6)]

        ###
        # compute CMV

        CMV = get_CMV(num_points, parameters, points)

        # the correctness of the content of CMV is verified using the ConditionEvaluations test case above
        # here we verify that the data is of correct format

        # check so that there are values
        self.assertTrue(len(CMV) > 0)

        # check so that vector contains only booleans
        for cond_val in CMV:
            self.assertTrue(isinstance(cond_val, bool))

    def test_decide(self):
        # Intent: Verify that the decide function behaves in accordance with the expected behaviour of well-known logical operations. Verify some simple base cases which are easily checked, and also for a higher-dimensional case

        LCM = [[ANDD]]
        PUV = [False]
        CMV = [False]
        launch, PUM, FUV = decide(CMV, LCM, PUV)

        self.assertTrue( launch )
        self.assertTrue( np.all(FUV) == launch )

        LCM = [[ANDD]]
        PUV = [True]
        CMV = [False]
        launch, PUM, FUV = decide(CMV, LCM, PUV)

        self.assertTrue( not launch )
        self.assertTrue( np.all(FUV) == launch )

        LCM = [[ANDD]]
        PUV = [True]
        CMV = [True]
        launch, PUM, FUV = decide(CMV, LCM, PUV)

        self.assertTrue( launch )
        self.assertTrue( np.all(FUV) == launch )

        LCM = [[ORR]]
        PUV = [True]
        CMV = [False]
        launch, PUM, FUV = decide(CMV, LCM, PUV)

        self.assertTrue( not launch )
        self.assertTrue( np.all(FUV) == launch )

        LCM = [[ORR]]
        PUV = [True]
        CMV = [True]
        launch, PUM, FUV = decide(CMV, LCM, PUV)

        self.assertTrue( launch )
        self.assertTrue( np.all(FUV) == launch )

        LCM = [[NOTUSED]]
        PUV = [True]
        CMV = [False]
        launch, PUM, FUV = decide(CMV, LCM, PUV)

        self.assertTrue( launch )
        self.assertTrue( np.all(FUV) == launch )

        LCM = [[ANDD, ANDD], [ANDD, ANDD]]
        PUV = [True, False]
        CMV = [False, False]
        launch, PUM, FUV = decide(CMV, LCM, PUV)

        self.assertTrue( not launch )
        self.assertTrue( np.all(FUV) == launch )

        LCM = [[ANDD, ANDD], [ANDD, ANDD]]
        PUV = [False, True]
        CMV = [False, False]
        launch, PUM, FUV = decide(CMV, LCM, PUV)

        self.assertTrue( not launch )
        self.assertTrue( np.all(FUV) == launch )

        LCM = [
            [ANDD, ANDD, ORR, ANDD, NOTUSED, NOTUSED],
            [ANDD, ANDD, ORR, ORR, NOTUSED, NOTUSED],
            [ORR, ORR, ANDD, ANDD, NOTUSED, NOTUSED],
            [ANDD, ORR, ANDD, ANDD, NOTUSED, NOTUSED],
            [NOTUSED, NOTUSED, NOTUSED, NOTUSED, NOTUSED, NOTUSED],
            [NOTUSED, NOTUSED, NOTUSED, NOTUSED, NOTUSED, NOTUSED]
        ]
        PUV = [True, True, True, True, True, True]
        CMV = [True, True, True, False, True, True]

        launch, PUM, FUV = decide(CMV, LCM, PUV)

        self.assertTrue( not launch )
        self.assertTrue( np.all(FUV) == launch )

if __name__ == '__main__':
    unittest.main()