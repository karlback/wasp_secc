
import numpy as np

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    def norm(self):
        return np.sqrt( self.x**2 + self.y**2 )
    def subtract(self, p):
        dx = self.x - p.x
        dy = self.y - p.y
        return Point(dx, dy)