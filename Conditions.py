
from smallestenclosingcircle import make_circle

import numpy as np

class Conditions:
    def __init__(self, parameters, num_points, points):
        self.parameters = parameters
        self.num_points = num_points
        self.points = points

class Cond0(Conditions):
    def eval(self):
        for i in range(self.num_points - 1):
            vect = self.points[i + 1].subtract(self.points[i])
            dist = vect.norm()
            if dist > self.parameters['length1']:
                return True
        return False

class Cond1(Conditions):
    def eval(self):
        for i in range(self.num_points - 2):
            c = make_circle([(self.points[i].x,self.points[i].y), (self.points[i+1].x,self.points[i+1].y), (self.points[i+2].x,self.points[i+2].y)])
            if c[2] <= self.parameters['radius1']:
                return True
        return False

class Cond2(Conditions):
    def eval(self):
        for i in range(self.num_points - 2):
            A = self.points[i + 1].subtract(self.points[i])
            B = self.points[i + 2].subtract(self.points[i + 1])
            theta = np.arccos((A.x * B.x + A.y * B.y) / np.sqrt(A.norm() ** 2 * B.norm() ** 2))
            if theta < np.pi - self.parameters['epsilon'] or theta > np.pi + self.parameters['epsilon']:
                return True
        return False

class Cond3(Conditions):
    def eval(self):
        for i in range(self.num_points - 2):
            A = self.points[i + 1].subtract(self.points[i])
            B = self.points[i + 2].subtract(self.points[i + 1])
            theta = np.arccos((A.x * B.x + A.y * B.y) / np.sqrt(A.norm() ** 2 * B.norm() ** 2))
            a = 0.5 * A.norm() * B.norm() * np.sin(theta)
            if a > self.parameters['area1']:
                return True
        return False

class Cond4(Conditions):
    def eval(self):
        for i in range(self.num_points - self.parameters['q_pts'] + 1):
            qs = self.points[i:i + self.parameters['q_pts']]
            quads = []
            for p in qs:
                if p.x >= 0 and p.y >= 0:
                    if 1 not in quads:
                        quads.append(1)
                elif p.x >= 0 and p.y < 0:
                    if 2 not in quads:
                        quads.append(2)
                elif p.x <= 0 and p.y <= 0:
                    if 3 not in quads:
                        quads.append(3)
                elif p.x <= 0 and p.y > 0:
                    if 4 not in quads:
                        quads.append(4)
            if len(quads) > self.parameters['quads']:
                return True
        return False

class Cond5(Conditions):
    def eval(self):
        for i in range(self.num_points - 1):
            if self.points[i + 1].x - self.points[i].x < 0:
                return True
        return False