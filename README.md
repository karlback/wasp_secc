# Launch Interceptor Program

Project work as part of the WASP course Software Engineering and Cloud Computing. Note that this repository only contains partial functionality of the original purpose, and is not suitable for an applied setting. Usage is at own risk (in other words, wear a helmet).

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them

```
Python 2.7+
```

### Usage

The software implements the function DECIDE according to the [software specification sheet](https://www.monperrus.net/martin/decide.pdf), the naming conventions of which we adopt for the rest of this document.

The functionality is accessed by running

```
python ./decide.py
```

The input parameters NUMPOINTS, POINTS, PARAMETERS, LCM and PUV are manually set at the top of ./decide.py

The outputs LAUNCH, CMV, PUM and FUV are written to the standard output channel

## Running the tests

The test suite is implemented in ./test.py using the Python unittest package, please see the [official documentation](https://docs.python.org/3/library/unittest.html) for usage instructions.

The complete test suite is run with the following command

```
./test.py
```

There exists an availability of scripts for automated testing. If this is required, it is recommended to use Git hooks.

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning.

## Authors

* **Karl Bäckström** - *Initial work*

## License

This project is licensed under the MIT License.

## Acknowledgments

* WASP - The Wallenberg AI, Autonomous Systems and Software Program
