
from decide import *
from Geometry import Point

parameters = {
    'length1': 2,
    'radius1': 2,
    'epsilon': 0.001,
    'area1': 1.5,
    'q_pts': 3,
    'quads': 2
}

num_points = 5
points = [ Point(1,7), Point(2,5), Point(3,1), Point(4,3), Point(5,6) ]
LCM = [
    [ANDD, ANDD, ORR, ANDD, NOTUSED, NOTUSED],
    [ANDD, ANDD, ORR, ORR, NOTUSED, NOTUSED],
    [ORR, ORR, ANDD, ANDD, NOTUSED, NOTUSED],
    [ANDD, ORR, ANDD, ANDD, NOTUSED, NOTUSED],
    [NOTUSED, NOTUSED, NOTUSED, NOTUSED, NOTUSED, NOTUSED],
    [NOTUSED, NOTUSED, NOTUSED, NOTUSED, NOTUSED, NOTUSED]
]
PUV = [False, False, True, True, True, True]

###
# compute CMV

CMV = get_CMV(num_points, parameters, points)

out, PUM, FUV = decide(CMV, LCM, PUV)

print out
print CMV
print PUM
print FUV